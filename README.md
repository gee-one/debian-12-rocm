# Installing ROCM on Bookworm/Debian 12

These are some steps to get ROCM working on Debian 12.  There is a little handwaving to complete the installation since we will make a fake package to satify one of the package dependencies.

ROCM is AMD's platform for GPU computation.  AMD provides installation instructions for RHEL, CentoOS, SLES, and Ubuntu, but not for Debian directly.  The Ubuntu installation provides a framework for Debian, but there are some package dependency and kernel version differences.

Currently, the latest ROCM version is 6.1.2 and Debian version is 12.5.

Note that ROCM might need over 30GB of drive space.  The linux kernel build could also use over 30GB.


## Overview

### Install compatible kernel
ROCM uses a DKMS module, which is installable on the Ubuntu kernel versions.  The default Debian kernel versions are slightly different, so the DKMS module installation in Debian is not completely clean.  DKMS might install the module with some errors, but it's not clear if it works well with ROCM.  Instead, we can compile and install the Ubuntu kernel on the Debian machine.

### Workaround for package dependencies
ROCM requires packages that are in the Ubuntu and Debian repos, however the package versions are different enough that apt won't install the dependencies from Debian alone.  We can build a fake package that provides the required packages as a workaround.

There doesn't seem to be any breaking changes between Python 3.10 and 3.11.  However, this is not fully tested, so proceed with caution.  If 3.10 is indeed required, it might be enough to install it locally for a single user, or build it from source.


### Install ROCM
Finally, we can install ROCM.  This guide will outline the package manager installation, although it can probably be adapted for use with the AMDGPU installer.


## Related Documentation

AMD ROCM docs

https://rocm.docs.amd.com/en/latest/

https://rocm.docs.amd.com/en/docs-6.1.2/


## Install a compatible kernel

We will build the compatible Ubuntu kernel (6.5) from source and install it on our Debian machine.  Debian kernel tooling is very mature and provides tools that make it quite easy to build the kernel.

https://wiki.debian.org/BuildADebianKernelPackage

We need the linux source for the Ubuntu 6.5 kernel.  We also need a kernel config, which we can get from one of the Ubuntu packages.

Build time can range from 5 mins to multiple hours, depending on your computer.


### Install dependencies
```
apt install  build-essential linux-source bc kmod cpio flex libncurses5-dev libelf-dev libssl-dev dwarves bison rsync bc debhelper
```

### Make a working directory

```
mkdir /opt/debian-12-rocm
cd /opt/debian-12-rocm
```

### Get the Ubuntu kernel source and extras.

These packages can be found at https://packages.ubuntu.com and by searching for the hardware entitlement kernels (HWE).

```
wget http://security.ubuntu.com/ubuntu/pool/main/l/linux-hwe-6.5/linux-source-6.5.0_6.5.0-35.35~22.04.1_all.deb
wget http://security.ubuntu.com/ubuntu/pool/main/l/linux-hwe-6.5/linux-modules-6.5.0-35-generic_6.5.0-35.35~22.04.1_amd64.deb
```

### Extract the source and config file
```
apt install ./linux-source-6.5.0_6.5.0-35.35~22.04.1_all.deb
ar x linux-modules-6.5.0-35-generic_6.5.0-35.35~22.04.1_amd64.deb  data.tar.zst
tar xf data.tar.zst --wildcards ./boot/config*
sed -e 's/CONFIG_SYSTEM_TRUSTED_KEYS=".*"/CONFIG_SYSTEM_TRUSTED_KEYS=""/g' -e 's/CONFIG_SYSTEM_REVOCATION_KEYS=".*"/CONFIG_SYSTEM_REVOCATION_KEYS=""/g' boot/config-6.5.0-35-generic > /usr/src/linux-source-6.5.0/.config
cd /usr/src/linux-source-6.5.0
tar xf linux-source-6.5.0.tar.bz2 -C ../
nice make -j`nproc` bindeb-pkg 
apt install ../linux-headers-6.5.13_6.5.13-1_amd64.deb  ../linux-image-6.5.13_6.5.13-1_amd64.deb
```


Reboot to use the new kernel

Check the kernel version after rebooting to make sure it is 6.5.
```
uname -r
```



Optional clean up since the kernel build directory will be over 30GB.
```
cd /usr/src/linux-source-6.5.0
make distclean
```

## Workaround for package dependencies

### Build fake package to satisfy libpython3.10 dependency

We will build a fake package to satify the ROCM install dependencies using equivs.

More info about equivs can be found here - https://debian-handbook.info/browse/stable/sect.building-first-package.html

```
apt install equivs
cd /opt/debian-12-rocm
cat > libpython-fake.equivs << EOF
### Commented entries have reasonable defaults.
### Uncomment to edit them.
# Source: <source package name; defaults to package name>
Section: misc
Priority: optional
# Homepage: <enter URL here; no default>
Standards-Version: 3.9.2

Package: libpython3.10-fake
Version: 3.10.12-1~22.04.3
# Maintainer: Your Name <yourname@example.com>
# Pre-Depends: <comma-separated list of packages>
Depends: libpython3-stdlib
# Recommends: <comma-separated list of packages>
# Suggests: <comma-separated list of packages>
Provides: libpython3.10
# Replaces: <comma-separated list of packages>
# Architecture: all
# Multi-Arch: <one of: foreign|same|allowed>
# Copyright: <copyright file; defaults to GPL2>
# Changelog: <changelog file; defaults to a generic changelog>
# Readme: <README.Debian file; defaults to a generic one>
# Extra-Files: <comma-separated list of additional files for the doc directory>
# Links: <pair of space-separated paths; First is path symlink points at, second is filename of link>
# Files: <pair of space-separated paths; First is file to include, second is destination>
#  <more pairs, if there's more than one file to include. Notice the starting space>
Description: fake package to provide libpython3.10 for rocm
  fake package to provide libpython3.10 for rocm
EOF

equivs-build libpython-fake.equivs
apt install ./libpython3.10-fake_3.10.12-1~22.04.3_all.deb
```


## Install ROCM

We are following the native package manager installation found here - https://rocm.docs.amd.com/projects/install-on-linux/en/latest/how-to/native-install/ubuntu.html

This is the method that doesn't install the amdgpu-install package.

### Install ROCM

```
apt install sudo

# Make the directory if it doesn't exist yet.
# This location is recommended by the distribution maintainers.
sudo mkdir --parents --mode=0755 /etc/apt/keyrings

# Download the key, convert the signing-key to a full
# keyring required by apt and store in the keyring directory
wget https://repo.radeon.com/rocm/rocm.gpg.key -O - | \
    gpg --dearmor | sudo tee /etc/apt/keyrings/rocm.gpg > /dev/null

echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/rocm.gpg] https://repo.radeon.com/amdgpu/6.1.2/ubuntu jammy main" \
    | sudo tee /etc/apt/sources.list.d/amdgpu.list
sudo apt update

echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/rocm.gpg] https://repo.radeon.com/rocm/apt/6.1.2 jammy main" \
    | sudo tee --append /etc/apt/sources.list.d/rocm.list
echo -e 'Package: *\nPin: release o=repo.radeon.com\nPin-Priority: 600' \
    | sudo tee /etc/apt/preferences.d/rocm-pin-600
sudo apt update

sudo apt install amdgpu-dkms
sudo reboot

sudo apt install rocm

```

### Post-installation instructions

Don't forget the post-install instuctions found here - https://rocm.docs.amd.com/projects/install-on-linux/en/latest/how-to/native-install/post-install.html


```
sudo tee --append /etc/ld.so.conf.d/rocm.conf <<EOF
/opt/rocm/lib
/opt/rocm/lib64
EOF
sudo ldconfig

export PATH=$PATH:/opt/rocm-6.1.2/bin

dkms status

/opt/rocm-6.1.2/bin/rocminfo
/opt/rocm-6.1.2/bin/clinfo
```

There is also a command from the quickstart guide to add non-root users to the correct groups. -https://rocm.docs.amd.com/projects/install-on-linux/en/latest/tutorial/quick-start.html

```
sudo usermod -a -G render,video $LOGNAME # Add the current user to the render and video groups
```

This last command isn't in the ROCM instructions, but it will make the PATH change persistant.  Adjust user home dir as needed.

```
echo 'export PATH=$PATH:/opt/rocm-6.1.2/bin' >> ~/.bashrc
```

### Clean up

Aside from cleaning up the kernel source in /usr/src/linux-6.5.0, everything in /opt/debian-12-rocm can be deleted as well.
